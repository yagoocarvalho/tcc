from tweet_online_classifier.leia.leia import SentimentIntensityAnalyzer

analyser = SentimentIntensityAnalyzer()


def sentiment_analyzer_scores(sentence):
    score = analyser.polarity_scores(sentence, debug=True)
    return score


if __name__ == "__main__":
    frases = [
        "Acho o BBB quase legal!",
        "Amo BBB!",
        "Amo levemente o BBB!",
        "Odeio um pouco o BBB!",
        "BBB é normal"
    ]
    for frase in frases:
        print(frase + " \t\t " + str(sentiment_analyzer_scores(frase)))
