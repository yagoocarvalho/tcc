import pandas as pd
from tweet_online_classifier.leia.leia import SentimentIntensityAnalyzer

analyser = SentimentIntensityAnalyzer()


def sentiment_analyzer_scores(sentence):
    score = analyser.polarity_scores(sentence, debug=True)
    return score

if __name__ == "__main__":
    df = pd.read_csv("fifty_sentences.csv")
    df['score'] = df['text'].apply(lambda x: sentiment_analyzer_scores(x)['compound'])
    df_neutral = df[df['score'] == 0.0]
    df_neutral[['text', 'score']].to_excel("neutral_sentences_fixed.xlsx", index=False)
    df[['text', 'score']].to_excel("fifty_sentences_result_fixed.xlsx", index=False)
