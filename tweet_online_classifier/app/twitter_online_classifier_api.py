# Import dependencies
import uvicorn
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from fastapi.middleware.gzip import GZipMiddleware
from starlette.responses import RedirectResponse
from tweet_online_classifier.app.api.v1 import subjects as subjects_v1
from tweet_online_classifier.app.api.v1 import tweets as tweets_v1
from tweet_online_classifier.app.core.mongodb_utils import connect_to_mongo, close_mongo_connection

# Create app
app = FastAPI(title='Twitter Online Classifier', version='/0.0.1',
              docs_url="/api/docs", redoc_url="/api/redoc",
              openapi_url="/api/openapi.json")

# Add mongo connection
app.add_event_handler("startup", connect_to_mongo)
app.add_event_handler("shutdown", close_mongo_connection)

responses = {
    400: {"description": "Bad Request"},
    401: {"description": "Unauthorized"},
    404: {"description": "Not Found"},
    500: {"description": "Internal Server Error"}
}

# Subjects
app.include_router(subjects_v1.router,
                   prefix="/api/subjects",
                   tags=["Subjects"],
                   responses=responses)

# Tweets
app.include_router(tweets_v1.router,
                   prefix="/api/tweets",
                   tags=["Tweets"],
                   responses=responses)


# Add CORS middleware
app.add_middleware(
    CORSMiddleware,
    allow_origins=['*', 'http://localhost:5000', 'https://localhost:5000'],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

# Add compression middleware. Compress requests greater then 100KB
app.add_middleware(GZipMiddleware, minimum_size=1024 * 100)


# Set / route to redirect to /docs
@app.get("/api")
async def main():
    response = RedirectResponse(url="/api/docs")
    return response


# Set main script for debug purposes
if __name__ == "__main__":
    uvicorn.run("twitter_online_classifier_api:app", host="0.0.0.0", port=8000, reload=True, workers=1)
