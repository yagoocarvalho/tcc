from motor.motor_asyncio import AsyncIOMotorClient
from tweet_online_classifier.constants import (MONGO_CONNECTION_STRING, MAX_CONNECTIONS_COUNT, MIN_CONNECTIONS_COUNT)
from tweet_online_classifier.app.core.mongodb import db


async def connect_to_mongo():
    db.client = AsyncIOMotorClient(str(MONGO_CONNECTION_STRING),
                                   maxPoolSize=MAX_CONNECTIONS_COUNT,
                                   minPoolSize=MIN_CONNECTIONS_COUNT)


async def close_mongo_connection():
    db.client.close()
