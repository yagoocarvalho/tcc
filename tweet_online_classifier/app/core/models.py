import datetime
from typing import List, Optional
from pydantic import BaseModel


class UserModel(BaseModel):
    username: str
    password: str


class SubjectObject(BaseModel):
    subject: str


class BaseResponseModel(BaseModel):
    message: str


class SubjectsResponse(BaseResponseModel):
    subjects: List[str]


class TweetsModel(BaseModel):
    text: str
    date: datetime.datetime
    subject: str
    score: Optional[float]

class TweetsResponseModel(BaseResponseModel):
    tweets: List[TweetsModel]
