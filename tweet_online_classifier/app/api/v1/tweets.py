import pandas as pd
from motor.motor_asyncio import AsyncIOMotorClient
from fastapi import APIRouter, status, Depends, Query
from tweet_online_classifier.app.core.models import TweetsResponseModel, TweetsModel
from tweet_online_classifier.constants import DATABASE_NAME, TWEETS_COLLECTION_NAME
from tweet_online_classifier.app.core.mongodb import get_database
from tweet_online_classifier.app.core.security import get_current_username
from tweet_online_classifier.nlp import nlp

router = APIRouter()


@router.get("/list", response_model=TweetsResponseModel, status_code=status.HTTP_200_OK)
async def list_tweets(
    *,
    subject: str = Query(..., example="bolsonaro"),
    mongo_client: AsyncIOMotorClient = Depends(get_database),
    # username: str = Depends(get_current_username)
):

    """ Create a subject
    """
    cursor = mongo_client[DATABASE_NAME][TWEETS_COLLECTION_NAME].find({"subject": subject})
    tweets = []
    async for record in cursor:
        tweet = TweetsModel(
            text=record['text'],
            date=pd.to_datetime(record['created_at']),
            subject=record['subject'],
            score=record.get('score')
        )
        if not tweet.score:
            tweet.score = nlp.sentiment_analyzer_scores(tweet.text)["compound"]
        tweets.append(tweet)
    return TweetsResponseModel(message="OK", tweets=tweets)
