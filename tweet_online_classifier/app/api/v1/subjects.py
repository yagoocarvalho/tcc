from motor.motor_asyncio import AsyncIOMotorClient
from fastapi import APIRouter, Body, status, Depends
from tweet_online_classifier.app.core.models import SubjectObject, BaseResponseModel, SubjectsResponse
from tweet_online_classifier.constants import DATABASE_NAME, SUBJECT_COLLECTION_NAME
from tweet_online_classifier.app.core.mongodb import get_database
from tweet_online_classifier.app.core.security import get_current_username
from tweet_online_classifier.tweet_reader.tweet_utils import save_tweets_to_db

router = APIRouter()


@router.post("/create", response_model=BaseResponseModel, status_code=status.HTTP_201_CREATED)
async def create_subject(
    *,
    subject: SubjectObject = Body(...),
    mongo_client: AsyncIOMotorClient = Depends(get_database),
    # username: str = Depends(get_current_username)
):

    """ Create a subject
    """
    row = await mongo_client[DATABASE_NAME][SUBJECT_COLLECTION_NAME].insert_one(subject.dict())
    save_tweets_to_db(subject.subject)
    return BaseResponseModel(message="OK")


@router.get("/list", response_model=SubjectsResponse, status_code=status.HTTP_200_OK)
async def list_subjects(
    *,
    mongo_client: AsyncIOMotorClient = Depends(get_database),
    # username: str = Depends(get_current_username)
):

    """ List subjects
    """
    cursor = mongo_client[DATABASE_NAME][SUBJECT_COLLECTION_NAME].find({})
    subjects = []
    async for record in cursor:
        subjects.append(record['subject'])
    return SubjectsResponse(message="OK", subjects=subjects)
