import pandas as pd
from tqdm import tqdm
from pymongo import MongoClient
from tweet_online_classifier.constants import (MONGO_CONNECTION_STRING, DATABASE_NAME, SUBJECT_COLLECTION_NAME,
                                               TWEETS_COLLECTION_NAME)
from tweet_online_classifier.tweet_reader.twitter_api_reader import TwitterApiReader
from tweet_online_classifier.nlp import nlp

mongo_client = MongoClient(MONGO_CONNECTION_STRING)
reader = TwitterApiReader()

def save_tweets_to_db(subject_in: str = None):
    subjects_collection = mongo_client.get_database(DATABASE_NAME).get_collection(SUBJECT_COLLECTION_NAME)
    tweets_collection = mongo_client.get_database(DATABASE_NAME).get_collection(TWEETS_COLLECTION_NAME)
    subjects = subjects_collection.find({})
    for record in subjects:
        subject = record['subject']
        if subject_in and subject != subject_in:
            continue
        print(f"Getting tweets for {subject}")
        last_tweet_id = record.get('last_tweet_id')
        try:
            df = reader.get_tweets_search_recent(query=f'"{subject}"',
                                                 since_id=last_tweet_id,
                                                 n_tweets=1000)
        except:
            df = reader.get_tweets_search_recent(query=f'"{subject}"',
                                                 n_tweets=1000)
        df['subject'] = subject
        if df.empty:
            continue
        df['score'] = df['text'].apply(lambda x: nlp.sentiment_analyzer_scores(x)["compound"])
        df['created_at'] = pd.to_datetime(df['created_at'])
        print(f"Updating DB with tweets for {subject}")
        resp = tweets_collection.insert_many(df.to_dict("records"))
        last_tweet_id = df['id'].max()
        update_last_tweet_id(subjects_collection=subjects_collection, subject=subject, last_tweet_id=last_tweet_id)


def update_last_tweet_id(subjects_collection, subject, last_tweet_id):
    subjects_collection.update_one({"subject": subject}, {'$set': {"last_tweet_id": last_tweet_id}}, upsert=False)


def update_db():
    tweets_collection = mongo_client.get_database(DATABASE_NAME).get_collection(TWEETS_COLLECTION_NAME)
    total_count = tweets_collection.count_documents({})
    for record in tqdm(tweets_collection.find({}), total=total_count):
        record['score'] = nlp.sentiment_analyzer_scores(record['text'])["compound"]
        tweets_collection.update_one({"_id": record['_id']}, {"$set": record})

if __name__ == "__main__":
    update_db()
    # import time
    # while True:
    #     save_tweets_to_db()
    #     time.sleep(30)
