import time
import datetime
import pandas as pd
import requests_html
from retry import retry
from urllib import response
from typing import Dict, List
from tweet_online_classifier.constants import TWITTER_API_TOKEN


class TwitterApiReader():
    def __init__(self,
                 token: str = TWITTER_API_TOKEN):
        self.token = token
        self.requests = requests_html.HTMLSession()
        self.requests.headers = {
            'Authorization': f'Bearer {token}'
        }

    def _datetime_to_str(self,
                         dt: datetime,
                         ):
        return dt.strftime("%Y-%m-%dT%H:%M:%Sz")

    def _params_dict_to_query_str(self, params: Dict[str, str]):
        return '&'.join([f'{k}={v}' for k, v in params.items()])

    @retry(tries=5, delay=2, backoff=2)
    def get_page_content(
        self,
        url: str
    ) -> response:
        resp = self.requests.get(url)
        time.sleep(2)
        if resp.status_code == 400 or resp.status_code == 200:
            return resp
        
        if resp.status_code != 200:
            raise Exception(f'status code is {resp.status_code}, {str(resp.text)}')
        
        return resp

    def get_tweets_search_recent(self,
                                 query: str,
                                 n_tweets: int = 20,
                                 since_id: int = None,
                                 start_time: datetime = None,
                                 end_time: datetime = None,
                                 params: Dict[str, str] = {},
                                 max_results: int = 10,
                                 user_fields: List[str] = ['name','username'],
                                 expansions: List[str] = ['referenced_tweets.id','author_id'],
                                 tweet_fields: List[str] = ['author_id','public_metrics','created_at'],
                                ):
        url_tweets_search_recent = "https://api.twitter.com/2/tweets/search/recent"

        _params = {
            'query': query,
            'max_results': max_results,
        }

        if len(tweet_fields) != 0:
            _params['tweet.fields'] = ','.join(tweet_fields)

        if len(expansions) != 0:
            _params['expansions'] = ','.join(expansions)

        if len(user_fields) != 0:
            _params['user.fields'] = ','.join(user_fields)

        if since_id is not None:
            _params['since_id'] = since_id

        if start_time:
            _params['start_time'] = self._datetime_to_str(start_time)
        if end_time:
            _params['end_time'] = self._datetime_to_str(end_time)

        if params:
            for k, v in params:
                _params[k] = v

        req_url_no_pagination = url_tweets_search_recent + '?' + self._params_dict_to_query_str(_params)
        req_url = req_url_no_pagination
        paginate = True
        n_tweets_result = 0
        dfs = []
        includes = []
        n_pages = round(n_tweets / max_results)
        page = 0
        while paginate:
            page += 1
            print(f"Getting page {page} of {n_pages}")
            resp = self.get_page_content(req_url)

            if resp.status_code != 200:
                raise Exception(f'status code is {resp.status_code}, {str(resp.text)}')

            if (resp.status_code == 200) and ('data' not in resp.json()) and (resp.json()['meta']['result_count'] == 0):
                if len(dfs) == 0:
                    return pd.DataFrame(), [resp.json()['meta']], {}
                else:
                    return pd.concat(dfs), [resp.json()['meta']], {}

            df = pd.DataFrame(resp.json()['data'])
            dfs.append(df)
            meta = resp.json()['meta']
            n_tweets_result += len(df)
            if 'includes' in resp.json().keys():
                includes.append(resp.json()['includes'])

            if n_tweets_result >= n_tweets:
                paginate = False
            elif 'next_token' in meta.keys():
                req_url = req_url_no_pagination + f"&next_token={meta['next_token']}"
            else:
                paginate = False
        df = pd.concat(dfs)
        df['created_at'] = pd.to_datetime(df['created_at'], format="%Y-%m-%dT%H:%M:%S.%fz")
        df.reset_index(drop=True, inplace=True)
        return df


if __name__ == "__main__":
    twitterapi = TwitterApiReader(token=TWITTER_API_TOKEN)
    df0   = twitterapi.get_tweets_search_recent(query='"jogo" lang:pt', n_tweets=50)
    df0.to_csv("fifty_sentences.csv")
    
