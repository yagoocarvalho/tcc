import os
from dotenv import load_dotenv

load_dotenv('.env')

TWITTER_API_TOKEN = os.environ["TWITTER_API_TOKEN"]
MONGO_CONNECTION_STRING = os.environ["MONGO_CONNECTION_STRING"]
MAX_CONNECTIONS_COUNT = 10
MIN_CONNECTIONS_COUNT = 1
DATABASE_NAME = "tweet_online_classifier"
SUBJECT_COLLECTION_NAME = "subjects"
TWEETS_COLLECTION_NAME = "tweets"
USER = os.environ["USER"]
PASS = os.environ["PASS"]
