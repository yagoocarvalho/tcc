import requests
import datetime
import pandas as pd
import plotly.express as px
from dash.exceptions import PreventUpdate
from dash import Dash, dcc, html, Input, Output, State

SUBJECTS_API_URL = "http://localhost:8000/api/subjects/list"
CREATE_SUBJECT_API_URL = "http://localhost:8000/api/subjects/create"
TWEETS_API_URL = "http://localhost:8000/api/tweets/list?subject="

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

app = Dash(__name__, external_stylesheets=external_stylesheets)


def load_subjects() -> pd.DataFrame():
    return pd.DataFrame({"subjects": requests.get(SUBJECTS_API_URL).json()['subjects']})


def load_tweets(subject: str, return_df: bool = False, daily_agg: bool = True):
    dff = pd.DataFrame(requests.get(TWEETS_API_URL + subject).json()['tweets'])
    if dff.empty:
        raise PreventUpdate
    dff.sort_values('date', inplace=True, ascending=False)
    dff_plot = dff[['date', 'score']].copy()
    if daily_agg:
        dff_plot['date'] = dff_plot['date'].apply(
            lambda x: datetime.datetime.strptime(x, "%Y-%m-%dT%H:%M:%S").replace(hour=0, minute=0, second=0))
    else:
        dff_plot['date'] = dff_plot['date'].apply(
            lambda x: datetime.datetime.strptime(x, "%Y-%m-%dT%H:%M:%S").replace(minute=0, second=0))
    dff_plot = dff_plot.groupby('date').mean().reset_index()

    fig = px.line(dff_plot,
                  x='date',
                  y='score',
                  labels={
                      'date': 'Date',
                      'score': 'Score'
                  },
                  markers=True)
    if return_df:
        return fig, dff
    return fig


def load_tweets_table(dff: pd.DataFrame = None, subject: str = None):
    if subject:
        dff = pd.DataFrame(requests.get(TWEETS_API_URL + subject).json()['tweets'])
    dff.sort_values('date', inplace=True, ascending=False)
    table_rows = [html.Tr([
        html.Th("Date"),
        html.Th("Text"),
        html.Th("Score"),
    ])]
    for _, row in dff.iterrows():
        table_rows.append(html.Tr([
            html.Td(row['date']),
            html.Td(row['text']),
            html.Td(row['score']),
        ]))
    return table_rows


app.layout = html.Div([
    html.Div([
        dcc.Loading([
            html.Div([
                "New Subject: ",
                dcc.Input(id='new-subject', type='text', placeholder='New Subject'),
                html.Button(children='Submit Subject', id="submit-subject")
            ], style={'width': '100%', 'display': 'inline-block', 'textAlign': 'center'})
        ]),
        html.Br(),
        html.Br(),
        html.H3("Select a subject:"),
        html.Div([
            dcc.Dropdown(
                options=load_subjects()['subjects'].unique(),
                value='bolsonaro',
                id='subject',
            ),
            dcc.RadioItems(
                options=['Hourly', 'Daily'],
                value='Daily',
                id='daily',
                inline=True
            )
        ], style={'width': '100%', 'display': 'inline-block', 'textAlign': 'center'}),
    ]),
    html.Br(),
    dcc.Loading([
        html.H3(children="Bolsonaro", id='graph-title', style={'textAlign': 'center'}),
        dcc.Graph(id='indicator-graphic', figure=load_tweets('bolsonaro'))
    ]),
    html.Br(),
    dcc.Loading([
        html.H3(children="Tweets", style={'textAlign': 'center'}),
        html.Table(id='tweets-table', children=load_tweets_table(subject='bolsonaro'))
    ])
])


@app.callback(
    Output('indicator-graphic', 'figure'),
    Output('graph-title', 'children'),
    Output('subject', 'options'),
    Output('tweets-table', 'children'),
    Input('subject', 'value'),
    Input('daily', 'value'),
    Input('submit-subject', 'n_clicks'),
    State('new-subject', 'value'))
def update_graph(subject: str, daily: str, n_clicks, new_subject):
    if not subject and not new_subject:
        raise PreventUpdate
    if new_subject:
        fig, dff = load_tweets(new_subject, return_df=True, daily_agg=daily=="Daily")
    else:
        fig, dff = load_tweets(subject, return_df=True, daily_agg=daily=="Daily")
    
    table_rows = load_tweets_table(dff)

    return fig, subject.title(), load_subjects()['subjects'].unique(), table_rows


@app.callback(Output('new-subject', 'value'),
              Output('subject', 'value'),
              Input('submit-subject', 'n_clicks'),
              State('new-subject', 'value'))
def create_subject(n_clicks, subject: str):
    response = requests.post(CREATE_SUBJECT_API_URL, json={'subject': subject})
    if not response or response.status_code >= 400:
        raise PreventUpdate
    return "", subject


if __name__ == '__main__':
    app.run_server(debug=True)
