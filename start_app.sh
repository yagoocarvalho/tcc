#!/bin/bash

export FLASK_APP=tweet_online_classifier
export FLASK_ENV=development
pip install -e tweet_online_classifier
flask run