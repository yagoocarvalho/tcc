import os
from setuptools import setup, find_packages


# Utility function to read the README file.
# Used for the long_description.  It's nice, because now 1) we have a top level
# README file and 2) it's easier to type in the README file than to put a raw
# string in below ...
def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()


setup(name='tweet_online_classifier',
      version='0.0.1',
      author="Yago Carvalho",
      author_email="yago.meira@poli.ufrj.br",
      description="",
      url="",
      packages=find_packages(),
      python_requires='>3.6',
      install_requires=[
        'flask',
        'vaderSentiment',
        'requests-html',
        'python-dotenv',
        'pandas',
        'fastapi',
        'uvicorn',
        'pymongo',
        'motor',
        'dnspython',
        'langdetect'
      ],
      extras_require={},
      long_description=read('README.md'),
      )
